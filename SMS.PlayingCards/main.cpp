#include <conio.h>
#include <iostream>
#include <string>
using namespace std;
enum class Rank
{
	TWO =2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	Queen,
	KING,
	ACE,
};
enum class Suit
{
	Clubs,
	Hearts,
	Spades,
	Diamonds
};
struct Cards
{
	Rank Rank;
	Suit Suit;
};
void PrintCardInfo(Cards card)
{
	cout << "The ";
	switch (card.Rank)
	{
	case Rank::ACE: cout << "Ace"; break;
	case Rank::KING: cout << "King"; break;
	case Rank::Queen: cout << "Queen"; break;
	case Rank::JACK: cout << "Jack"; break;
	case Rank::TEN: cout << "Ten"; break;
	case Rank::NINE: cout << "Nine"; break;
	case Rank::EIGHT: cout << "Eight"; break;
	case Rank::SEVEN: cout << "Seven"; break;
	case Rank::SIX: cout << "Six"; break;
	case Rank::FIVE: cout << "Five"; break;
	case Rank::FOUR: cout << "Four"; break;
	case Rank::THREE: cout << "Three"; break;
	case Rank::TWO: cout << "Two"; break;
	};
	cout << " of ";
	switch (card.Suit)
	{
	case Suit::Clubs: cout << "Clubs\n"; break;
	case Suit::Spades: cout << "Spades\n"; break;
	case Suit::Hearts: cout << "Hearts\n"; break;
	case Suit::Diamonds: cout << "Diamonds\n"; break;
	}
}
 Cards HighCard(Cards c1, Cards c2)
{
	if (c1.Rank>c2.Rank)
	{
		return c1;
	}
	else
	{
		return c2;
	}
}
int main()
{
	Cards c1;
	c1.Rank = Rank::Queen;
	c1.Suit = Suit::Diamonds;
	Cards c2;
	c2.Rank = Rank::FIVE;
	c2.Suit = Suit::Diamonds;

	HighCard(c1, c2);
	PrintCardInfo(c1);
	(void)_getch();
	return 0;
}